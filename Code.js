const ONYX_SERVER = 'https://7824-157-39-6-79.in.ngrok.io/';

function onOpen(e) {
  SpreadsheetApp.getUi()
    .createAddonMenu()
    .addItem('Open', 'openBaseModal')
    .addToUi();
}

function onInstall(e) {
  onOpen(e);
}

function include(filename) {
  return HtmlService.createHtmlOutputFromFile(filename)
      .getContent();
}

function logMessage(message) {
  Logger.log('Call from client-side')
  Logger.log(message)
}

function getAuthenticationToken() {
  Logger.log('Calling function getAuthenticationToken()');
  var authToken = ScriptApp.getIdentityToken();
  var body = authToken.split('.')[1];
  var decoded = Utilities.newBlob(Utilities.base64Decode(body)).getDataAsString();
  var payload = JSON.parse(decoded);
  Logger.log('User Email: ' + payload.email);
  Logger.log('User Auth Token: ' + authToken);

  return authToken;
}

function registerOrLogInUserToOnyx(authToken) {
  Logger.log('Calling function registerOrLogInUserToOnyx()');

  // POST Request
  let url = ONYX_SERVER + '/onyx/auth/google/';
  let data = {
    'token': authToken
  };
  let options = {
    'method' : 'post',
    'contentType': 'application/json',
    'payload' : JSON.stringify(data)
  };
  let responseTxT = UrlFetchApp.fetch(url, options);
  let response = JSON.parse(responseTxT.getContentText());
  Logger.log(response);
  return response;
}

function processActiveSpreadsheet() {
  var sheet = SpreadsheetApp.getActiveSheet();
}

function openBaseModal(){
  getAuthenticationToken();
  openModalTemplate('App', ' ');
}

function openModalTemplate(htmlTemplate, templateTitle=' '){
  let ui = HtmlService
    .createTemplateFromFile(htmlTemplate)
    .evaluate()
    .setWidth(800)
    .setHeight(686)

  SpreadsheetApp.getUi().showModelessDialog(ui, templateTitle);
}

function fetchCroveTemplateDetail(templateID) {
  Logger.log('Calling function fetchCroveTemplateDetail()');

  // GET Request
  let url = 'https://v2.api.crove.app/api/integrations/external/templates/' + templateID + "/";
  let options = {
    'headers' : {
      'X-API-KEY' : '9e824151d4efe4d23545957030bdca785c4757320ac39f3e9b3fd6509a5807e8'
    },
    'method' : 'get',
    'contentType': 'application/json',
  };
  let responseTxT = UrlFetchApp.fetch(url, options);
  let response = JSON.parse(responseTxT.getContentText());
  Logger.log(response);
  return response;
}

function fetchCroveTemplates(apiKey) {
  
  Logger.log('Calling function fetchCroveTemplates()');

  // GET Request
  let url = 'https://v2.api.crove.app/api/integrations/external/templates/';
  let options = {
    'headers' : {
      'X-API-KEY' : apiKey
    },
    'method' : 'get',
    'contentType': 'application/json',
  };
  let responseTxT = UrlFetchApp.fetch(url, options);
  let response = JSON.parse(responseTxT.getContentText());
  Logger.log(response);
  return response;
}

// DATABASE

var server = "sql6.freemysqlhosting.net";
var port = 3306;
var db = "sql6514909";
var username = "sql6514909";
var pwd = "XQ9gY9msAe";

// Connect to MySQL.

function connectmysql() {
  let url = "jdbc:mysql://" + server+":"+port+"/"+db;
  let conn = Jdbc.getConnection(url, username, pwd);
  Logger.log(conn);
  createTable(conn, SpreadsheetApp.getActive().getId().toString());
  conn.close();
}

// Create Table

function createTable(conn, tableName) {
  let stmt = conn.createStatement();
  let query = 'CREATE TABLE ' + 'tableName' + ' (id INT(6), name VARCHAR(20), email VARCHAR(40));'
  console.log(query);
  stmt.execute(query);
  console.log("Table Created successfully");
}
